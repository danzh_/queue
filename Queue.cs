﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace CustomQueue
{
    public class Queue<T> : IEnumerable<T>
    {
        #region fields
        private T[] _items;
        private int count;
        private int head = 0;
        private int tail = 0;
        #endregion

        #region properties
        private int Capacity { get; set; }
        public int Count
        {
            get => count; private set
            {
                count = value;

                if (count > Capacity)
                {
                    var temp = new T[Capacity * 2];

                    for (int i = 0; i < Capacity; i++)
                    {
                        temp[i] = _items[(head + i) % Capacity];
                    }

                    head = 0;
                    tail = Capacity;
                    Capacity *= 2;
                    _items = temp;
                }
            }
        }
        #endregion

        #region ctors
        public Queue()
        {
            Capacity = 4;
            Count = 0;
            _items = new T[Capacity];
        } 
        public Queue(int capacity)
        {
            if (capacity < 0) throw new ArgumentOutOfRangeException();

            Capacity = capacity;
            Count = 0;
            _items = new T[Capacity];
        }
        #endregion

        #region methods
        public void Enqueue(T value)
        {
            Count++;
            _items[tail++ % Capacity] = value;
        }
        public T Peek()
        {
            return _items[head];
        }
        public T Dequeue()
        {
            if (Count == 0) throw new InvalidOperationException("В очереди нет элементов");

            Count--;

            return _items[head++ % Capacity];
        }
        #endregion

        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < Count; i++)
            {
                yield return _items[(head + i) % Capacity];
            }
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        public override string ToString()
        {
            return $"[{string.Join(", ", this)}]";
        }
    }
}
