﻿using System;
using System.Diagnostics;

namespace CustomQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            int count = 10000000;
            Stopwatch sw = new Stopwatch();
            Queue<int> Cq = new Queue<int>();
            System.Collections.Generic.Queue<int> MSq = new System.Collections.Generic.Queue<int>();

            Console.WriteLine("MSQueue\n\n");

            sw.Start();
            for (int i = 0; i < count; i++)
            {
                MSq.Enqueue(i);
            }
            sw.Stop();
            Console.WriteLine("Enqueue " + sw.Elapsed.Ticks);

            sw.Start();
            for (int i = 0; i < count; i++)
            {
                MSq.Dequeue();
            }
            sw.Stop();
            Console.WriteLine("Dequeue " + sw.Elapsed.Ticks);

            Console.ForegroundColor = ConsoleColor.Red;

            Console.WriteLine("\n\nCQueue\n\n");

            sw.Reset();

            sw.Start();
            for (int i = 0; i < count; i++)
            {
                Cq.Enqueue(i);
            }
            sw.Stop();
            Console.WriteLine("Enqueue " + sw.Elapsed.Ticks);

            sw.Reset();

            sw.Start();
            for (int i = 0; i < count; i++)
            {
                Cq.Dequeue();
            }
            sw.Stop();
            Console.WriteLine("Dequeue " + sw.Elapsed.Ticks);

            sw.Reset();

            Console.ReadKey();
        }
    }
}
